import typicode from './config.js'

const fetchAll = async () => {
  let result = await typicode.get(`todos/`)
  return result
}


const fetchById = async (id) => {
  let result = await typicode.get(`todos/${id}`)
  return result
}


const todos = {
  fetchAll,
  fetchById
}

export default todos
