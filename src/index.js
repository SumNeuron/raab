import api from './config'
import todos from './todos'

const typicode = {
  api,
  todos
}

export default typicode
