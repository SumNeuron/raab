import axios from 'axios'

const typicode = axios.create({
  baseURL: 'https://jsonplaceholder.typicode.com/'
});


export default typicode
