import async from 'rollup-plugin-async';
import babel from 'rollup-plugin-babel';
import uglify from 'rollup-plugin-uglify-es';
import minimist from 'minimist';

import pkg from './package.json';

const argv = minimist(process.argv.slice(2));

const config = {
  input: 'src/index.js',
  external: [
    'axios',
    // '@babel/runtime/regenerator',
    // '@babel/runtime/helpers/asyncToGenerator'
  ],
  output: {
    extend: true,
    name: pkg.name,
    exports: 'named',
    globals: {
      'axios': 'axios',
      // '@babel/runtime/regenerator': '_regeneratorRuntime',
      // '@babel/runtime/helpers/asyncToGenerator': '_asyncToGenerator',
    },
  },
  plugins: [
    async(),
    babel({
      exclude: 'node_modules/**',
      // runtimeHelpers: true,
      externalHelpers: true,
      plugins: [
        [
          'wildcard',
          {
            exts: [],
            nostrip: true,
          },
        ],
        '@babel/plugin-external-helpers',
        // '@babel/plugin-transform-runtime'
      ],
      presets: [
        // ["es2017", {modules: false},],
        ['@babel/preset-env', {modules: false,},],
      ],
    }),
  ],
};

// Only minify browser (iife) version
if (argv.format === 'iife') {
  config.plugins.push(uglify());
}

export default config;
